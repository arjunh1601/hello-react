import React, {Component} from 'react';
import {render} from 'react-dom';

let rootElement = document.getElementById('root');

class App extends Component {
  render() {
    return (
      <h1> Hello World </h1>
    )
  }
}

render(<App />, rootElement);
