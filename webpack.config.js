var webpack = require('webpack');
var path = require('path');

// Build Directory
// Webpack will put the bundled assets in this directory.
var BUILD_DIR = path.resolve(__dirname, 'build');

// App directory
// Our source code lies here.
var APP_DIR = path.resolve(__dirname, 'src');

// Webpack configuration
var config = {
  // Entry point for webpack.
  entry: APP_DIR + '/index.jsx',

  // Name and path of the of the output file.
  // bundle.js is the output file which will be placed in the build directory.
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js',
  },

  // Use babel for compile jsx code.
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel'
      }
    ]
  }
};

module.exports = config;